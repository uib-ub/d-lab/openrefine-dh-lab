OpenRefine (formerly Google Refine) is a powerful tool for working with messy data, including tools for cleaning, transforming from one format into another, extending with web services, and connecting to external data.

This repo contains a Vuepress SSG web app. It is a guide for working with data using OpenRefine. 
