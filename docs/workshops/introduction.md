# Introduction to OpenRefine 


**Workshops held:**

* 12. March 2019, by Tarje Sælen Lavik


## What is OpenRefine?

<img :src="$withBase('/OpenRefine.png')" alt="OpenRefine diamond" width="150" style="float: right;">

OpenRefine (formerly Google Refine) is a powerful tool for working with messy data, including tools for cleaning, transforming from one format into another, extending with web services, and connecting to external data.

Can OpenRefine be used to create data from scratch? Not really. OpenRefine is built to work with existing data, although projects can be enriched with outside data.

Download OpenRefine [here](http://openrefine.org/download.html). [Detailed install instructions](https://github.com/OpenRefine/OpenRefine/wiki/Installation-Instructions)

OpenRefine requires a working Java runtime environment, otherwise the program will not start. Upon launch, OpenRefine will run as a local server, opening in your computer's browser. As long as OpenRefine is running, you can point your browser at either http://127.0.0.1:3333/ or http://localhost:3333/ to use it, even in several browser tabs/windows.

## Comparative Advantage
Why Use OpenRefine vs. Excel or other tools?

* Easy
  * Menu driven faceting, and filtering
  * Clean up data inconsistencies using powerful clustering and edit algorithms
* Transforming Data
  * Advanced power comes from Regular Expressions implementation via GREL
  * Web scraping: API orchestration, HTML/JSON parsing
* Why Use OpenRefine?
  * You don’t want to be a data engineer, but you need to fix your data
  * Don’t have to deal with file handling
  * Don’t have to deal with iterating (looping logic)
  * Don’t have to write complex conditional statements
  * Don’t have to think hard to manipulate arrays
  * Or, you are a lazy programmer (i.e. a smart programmer)
  * Or, you are a subject matter expert and Refine is the right tool for the job
* Frequently, OpenRefine is simpler to use than Excel for data transformations

## Reproducible
Refine supports reproducible research.

* Undo: every step is recorded and can be undone.
* Share: your steps are recorded and can be shared in a JSON notation
* Rerun: did I say your steps are recorded? Not only that, but those “recordings” can be shared with others or re-run on future data.
* Projects: each OpenRefine “project” retains a history of all your steps. The project can be exported and subsquently imported to other instances of OpenRefine.
* Exporting: There is a powerful export mechanism that allows you to build selective reports

## Saving data
Refine saves after all operations, but might, might lose your last edit if only the browser window is closed. Remember to quit Refine porperly!

**NB!** Windows: Control-C Mac: Click the OR app in the doc, invoke Quit

## Importing Data
From Refine's start screen, you can create projects from structured data files, continue working on past projects, and import projects that were exported out of OpenRefine. 

Refine can import TSV, CSV, Excel, XML, JSON, and Google Data documents as well as parse raw, unformatted data copied and pasted using the Clipboard function. 

[Example spreadsheet](./NJShipwrecks.csv)

**Web Addresses (URLs)**

`https://uib-ub.gitlab.io/workshops/openrefine-dh-lab/NJShipwrecks.csv`

## Overview 

<iframe src="//slides.com/tarjelavik/openrefine/embed" width="576" height="420" scrolling="no" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

## At the Laundromat

You can clean the data using the interface to remove trailing spaces, convert values to/from text, numbers, dates... You can alse use GREL (General Refine Expression Language) to do more complicated stuff.

### Cleaning strings

```js
// Replace stuff
value.replace(",", "")

// Remove trailing . without regex!
if(value.endsWith("."), replace(value, ".", ""), value)

// Filter with
^(\d.*,\d.)
// &
value.replace(",", ""), value)

// Split on space and get first value in array
value.split(/\s/)[0]

// Remove commas in number values
value.replace(/^(\d+),(\d+)/, "$1$2")

// Only get starting numbers from string
value.replace(/[^\d]/, "")

// Concat some string AND get a date
(cells.YEAR.value + "-" + cells.MNTH.value + "-" + value).toDate()
```

## Reconciliation
Lets match our things up with other things by using the awesome power of electricity!

Start by filtering down to just a handfull of rows. Then choose `Reconcile` -> `Start reconiliation` on the row in question. Choose `Wikidata (en)`.

Below is the complete history on how to reconcile a concept, `VESSEL TYPE`, with [Kulturnav.org](https://kulturnav.org) and extend our data with KN-data.

```json
[
  {
    "op": "core/recon",
    "description": "Reconcile cells in column VESSEL TYPE to type Q2235308",
    "columnName": "VESSEL TYPE",
    "config": {
      "mode": "standard-service",
      "service": "https://tools.wmflabs.org/openrefine-wikidata/en/api",
      "identifierSpace": "http://www.wikidata.org/entity/",
      "schemaSpace": "http://www.wikidata.org/prop/direct/",
      "type": {
        "id": "Q2235308",
        "name": "ship type"
      },
      "autoMatch": true,
      "columnDetails": [],
      "limit": 0
    },
    "engineConfig": {
      "facets": [
        {
          "type": "list",
          "name": "VESSEL TYPE",
          "expression": "value",
          "columnName": "VESSEL TYPE",
          "invert": false,
          "selection": [
            {
              "v": {
                "v": "Schooner",
                "l": "Schooner"
              }
            }
          ],
          "selectNumber": false,
          "selectDateTime": false,
          "selectBoolean": false,
          "omitBlank": false,
          "selectBlank": false,
          "omitError": false,
          "selectError": false
        },
        {
          "type": "list",
          "name": "FLAG",
          "expression": "value",
          "columnName": "FLAG",
          "invert": false,
          "selection": [
            {
              "v": {
                "v": "Denmark",
                "l": "Denmark"
              }
            }
          ],
          "selectNumber": false,
          "selectDateTime": false,
          "selectBoolean": false,
          "omitBlank": false,
          "selectBlank": false,
          "omitError": false,
          "selectError": false
        }
      ],
      "mode": "record-based"
    }
  },
  {
    "op": "core/extend-reconciled-data",
    "description": "Extend data at index 4 based on column VESSEL TYPE",
    "engineConfig": {
      "facets": [
        {
          "type": "list",
          "name": "VESSEL TYPE",
          "expression": "value",
          "columnName": "VESSEL TYPE",
          "invert": false,
          "selection": [
            {
              "v": {
                "v": "Schooner",
                "l": "Schooner"
              }
            }
          ],
          "selectNumber": false,
          "selectDateTime": false,
          "selectBoolean": false,
          "omitBlank": false,
          "selectBlank": false,
          "omitError": false,
          "selectError": false
        },
        {
          "type": "list",
          "name": "FLAG",
          "expression": "value",
          "columnName": "FLAG",
          "invert": false,
          "selection": [
            {
              "v": {
                "v": "Denmark",
                "l": "Denmark"
              }
            }
          ],
          "selectNumber": false,
          "selectDateTime": false,
          "selectBoolean": false,
          "omitBlank": false,
          "selectBlank": false,
          "omitError": false,
          "selectError": false
        }
      ],
      "mode": "record-based"
    },
    "columnInsertIndex": 4,
    "baseColumnName": "VESSEL TYPE",
    "endpoint": "https://tools.wmflabs.org/openrefine-wikidata/en/api",
    "identifierSpace": "http://www.wikidata.org/entity/",
    "schemaSpace": "http://www.wikidata.org/prop/direct/",
    "extension": {
      "properties": [
        {
          "name": "KulturNav-id",
          "id": "P1248"
        }
      ]
    }
  },
  {
    "op": "core/text-transform",
    "description": "Text transform on cells in column KulturNav-id using expression grel:\"http://kulturnav.org/\" + value",
    "engineConfig": {
      "facets": [
        {
          "type": "list",
          "name": "VESSEL TYPE",
          "expression": "value",
          "columnName": "VESSEL TYPE",
          "invert": false,
          "selection": [
            {
              "v": {
                "v": "Schooner",
                "l": "Schooner"
              }
            }
          ],
          "selectNumber": false,
          "selectDateTime": false,
          "selectBoolean": false,
          "omitBlank": false,
          "selectBlank": false,
          "omitError": false,
          "selectError": false
        },
        {
          "type": "list",
          "name": "FLAG",
          "expression": "value",
          "columnName": "FLAG",
          "invert": false,
          "selection": [
            {
              "v": {
                "v": "Denmark",
                "l": "Denmark"
              }
            }
          ],
          "selectNumber": false,
          "selectDateTime": false,
          "selectBoolean": false,
          "omitBlank": false,
          "selectBlank": false,
          "omitError": false,
          "selectError": false
        }
      ],
      "mode": "record-based"
    },
    "columnName": "KulturNav-id",
    "expression": "grel:\"http://kulturnav.org/\" + value",
    "onError": "keep-original",
    "repeat": false,
    "repeatCount": 10
  },
  {
    "op": "core/column-addition-by-fetching-urls",
    "description": "Create column kn-raw at index 5 by fetching URLs based on column KulturNav-id using expression grel:value",
    "engineConfig": {
      "facets": [
        {
          "type": "list",
          "name": "VESSEL TYPE",
          "expression": "value",
          "columnName": "VESSEL TYPE",
          "invert": false,
          "selection": [
            {
              "v": {
                "v": "Schooner",
                "l": "Schooner"
              }
            }
          ],
          "selectNumber": false,
          "selectDateTime": false,
          "selectBoolean": false,
          "omitBlank": false,
          "selectBlank": false,
          "omitError": false,
          "selectError": false
        },
        {
          "type": "list",
          "name": "FLAG",
          "expression": "value",
          "columnName": "FLAG",
          "invert": false,
          "selection": [
            {
              "v": {
                "v": "Denmark",
                "l": "Denmark"
              }
            }
          ],
          "selectNumber": false,
          "selectDateTime": false,
          "selectBoolean": false,
          "omitBlank": false,
          "selectBlank": false,
          "omitError": false,
          "selectError": false
        }
      ],
      "mode": "record-based"
    },
    "newColumnName": "kn-raw",
    "columnInsertIndex": 5,
    "baseColumnName": "KulturNav-id",
    "urlExpression": "grel:value",
    "onError": "set-to-blank",
    "delay": 5000,
    "cacheResponses": true,
    "httpHeadersJson": [
      {
        "name": "authorization",
        "value": ""
      },
      {
        "name": "user-agent",
        "value": "OpenRefine 3.1 [b90e413]"
      },
      {
        "name": "accept",
        "value": "application/ld+json"
      }
    ]
  },
  {
    "op": "core/column-addition",
    "description": "Create column vessel-desc at index 6 based on column kn-raw using expression grel:value.parseJson()[\"@graph\"][0].description",
    "engineConfig": {
      "facets": [
        {
          "type": "list",
          "name": "VESSEL TYPE",
          "expression": "value",
          "columnName": "VESSEL TYPE",
          "invert": false,
          "selection": [
            {
              "v": {
                "v": "Schooner",
                "l": "Schooner"
              }
            }
          ],
          "selectNumber": false,
          "selectDateTime": false,
          "selectBoolean": false,
          "omitBlank": false,
          "selectBlank": false,
          "omitError": false,
          "selectError": false
        },
        {
          "type": "list",
          "name": "FLAG",
          "expression": "value",
          "columnName": "FLAG",
          "invert": false,
          "selection": [
            {
              "v": {
                "v": "Denmark",
                "l": "Denmark"
              }
            }
          ],
          "selectNumber": false,
          "selectDateTime": false,
          "selectBoolean": false,
          "omitBlank": false,
          "selectBlank": false,
          "omitError": false,
          "selectError": false
        }
      ],
      "mode": "record-based"
    },
    "newColumnName": "vessel-desc",
    "columnInsertIndex": 6,
    "baseColumnName": "kn-raw",
    "expression": "grel:value.parseJson()[\"@graph\"][0].description",
    "onError": "set-to-blank"
  }
]
```

### Useful GREL commands when reconciling

```js
// Get the matched ID
cell.recon.match.id

// Get the matched URI
cell.recon.identifierSpace + cell.recon.match.id

// Get the Kulturnav description from the json-ld
value.parseJson()["@graph"][0].description
```


## Export
You can save and export the whole prosject, with all the edit history and all. Highly recommend you do this regularly. 

`Export` -> `Export project` -> `Export to Google Drive`


## Attribution

* [Get started with OpenRefine](http://miriamposner.com/classes/dh101f17/tutorials-guides/data-manipulation/get-started-with-openrefine/)
* [OpenRefine Workshop, DPLA Members Meeting, March 14, 2018](https://github.com/dpla/Metadata-Analysis-Workshop/blob/master/OpenRefine.md)
* [Cleaning Data with OpenRefine](https://libjohn.github.io/openrefine/)
* [OpenRefine Wiki](https://github.com/OpenRefine/OpenRefine/wiki)