module.exports = {
    title: 'OpenRefine 🎓 DH-Lab@UiB',
    description: 'Introduction workshop to OpenRefine 3.x',
    base: '/workshops/openrefine-dh-lab/',
    dest: 'public',
    themeConfig: {
      logo: 'OpenRefine.png',
      // Assumes GitHub. Can also be a full GitLab url.
      repo: 'https://gitlab.com/uib-ub/workshops/openrefine-dh-lab',
      // Customising the header label
      // Defaults to "GitHub"/"GitLab"/"Bitbucket" depending on `themeConfig.repo`
      repoLabel: 'Contribute!',
      // if your docs are not at the root of the repo:
      docsDir: 'docs',
      // if your docs are in a specific branch (defaults to 'master'):
      docsBranch: 'master',
      // defaults to false, set to true to enable
      editLinks: true,
      // custom text for edit link. Defaults to "Edit this page"
      editLinkText: 'Edit this page!',
      nav: [
        {
         text: 'tinyurl.com/dh-lab-uib',
         link: 'https://tinyurl.com/dh-lab-uib' 
        }
      ],
      sidebar: [
        ['/', 'Home'],
        {
          title: "Workshops",
          collapsable: false,
          children: [
            '/workshops/introduction'
          ]
        },
        '/use-cases-n-solutions.md'
      ]
    },
  }