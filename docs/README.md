# OpenRefine <img :src="$withBase('/OpenRefine.png')" alt="OpenRefine diamond" width="25" style="display: inline-block;"> DH-Lab@UiB

The [UiB Digital Humanities Network](https://www.uib.no/en/digitalhumanities) was established in December 2018, and includes researchers from all four departments in the Humanities Faculty and from Infomedia in the Social Sciences. The network plans to host events every two or three weeks in 2019. For updates, [subscribe to our mailing list](https://mailman.uib.no/listinfo/dighum-uib). 


## What is OpenRefine?

<img :src="$withBase('/OpenRefine.png')" alt="OpenRefine diamond" width="150" style="float: right;">

OpenRefine (formerly Google Refine) is a powerful tool for working with messy data, including tools for cleaning, transforming from one format into another, extending with web services, and connecting to external data.

Download OpenRefine [here](http://openrefine.org/download.html). [Detailed install instructions](https://github.com/OpenRefine/OpenRefine/wiki/Installation-Instructions)

## Contribute!

Help make this guide much, much better! We use [VuePress](https://vuepress.vuejs.org) to generate this page on [Gitlab.com](https://gitlab.com). Register an account there and start contributing!

Click `Contribute!` in the header or use `Edit this page` on the page you want to improve.

Have a use case and a solution? No solutions? Want help? Add it to this page [Use cases, problems and solutions](https://gitlab.com/uib-ub/workshops/openrefine-dh-lab/edit/master/docs/use-cases-n-solutions.md) to get feedback and/or help others!

We use [VuePress](https://vuepress.vuejs.org) to generate this page on Gitlab.com. Register an account there and start contributing!

## Great guides

* [Get started with OpenRefine](http://miriamposner.com/classes/dh101f17/tutorials-guides/data-manipulation/get-started-with-openrefine/)
* [OpenRefine Workshop, DPLA Members Meeting, March 14, 2018](https://github.com/dpla/Metadata-Analysis-Workshop/blob/master/OpenRefine.md)
* [Cleaning Data with OpenRefine](https://libjohn.github.io/openrefine/)
* [Official OpenRefine Wiki](https://github.com/OpenRefine/OpenRefine/wiki)