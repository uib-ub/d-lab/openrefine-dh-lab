# Use cases and solutions

You are welcome to add your use case, data and goal here! 

## 1: I want to do convert my data to RDF to be able query my data with SPARQL

My data have links to other datasets after being [reconciled](https://github.com/OpenRefine/OpenRefine/wiki/Reconciliation) with OpenRefine and now I want to convert my data to [RDF](https://en.wikipedia.org/wiki/Resource_Description_Framework). 

TODO: {Link to Dataset/project}

**Solution**

[Download](https://github.com/stkenny/grefine-rdf-extension/releases) and [install](https://github.com/stkenny/grefine-rdf-extension/wiki) [grefine-rdf-extension](https://github.com/stkenny/grefine-rdf-extension). This will add a GUI for building a template for converting your boring,old spreadsheet to a shiny, new graph.

TODO: Add better description on the datamodelling.

**By: Tarje Sælen Lavik**

## Template

```markdown
## number#: I want to do something to be able to find out X

{Description}

{Dataset}

{Solution}

**By: {Author}**
```